package br.com.julio.loja;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

import org.glassfish.grizzly.http.server.HttpServer;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.thoughtworks.xstream.XStream;

import br.com.julio.loja.modelo.Projeto;
import br.com.julio.servidor.Servidor;
import org.junit.Assert;

public class ProjetoTest {
	
private static HttpServer servidor;
	
	@BeforeClass
	public static void inicializarServidor(){
		servidor = Servidor.inicializarServidor();
	}
	
	@AfterClass
	public static void pararServidor(){
		servidor.shutdownNow();
	}
	
	@Test
	public void deveReceberXML(){
		Client client = ClientBuilder.newClient();
		WebTarget target = client.target("http://localhost:8080");
		String retorno = target.path("/projetos/1").request().get(String.class);
		System.out.println(retorno);
		Projeto projeto = (Projeto) new XStream().fromXML(retorno);
		Assert.assertEquals("Minha loja", projeto.getNome());
	}

}
