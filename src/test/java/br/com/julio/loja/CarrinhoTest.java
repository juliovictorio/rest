package br.com.julio.loja;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Feature;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.filter.LoggingFilter;
import org.glassfish.jersey.logging.LoggingFeature;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.thoughtworks.xstream.XStream;

import br.com.julio.loja.modelo.Carrinho;
import br.com.julio.loja.modelo.Produto;
import br.com.julio.servidor.Servidor;

public class CarrinhoTest {
	private static HttpServer servidor;
	private static Client client;
	
	@BeforeClass
	public static void inicializarServidor(){
		servidor = Servidor.inicializarServidor();
		
		ClientConfig config = new ClientConfig();
		Logger logger = Logger.getLogger(CarrinhoTest.class.getName());
		Feature feature = new LoggingFeature(logger, Level.INFO, null, null);
		config.register(feature);
		
		//client = ClientBuilder.newClient(); //simples
		client = ClientBuilder.newClient(config);
	}
	
	@AfterClass
	public static void pararServidor(){
		servidor.shutdownNow();
	}

	
	@Test
	public void testaQueBuscarUmCarrinhoTrazOCarrinhoEsperado() {
		WebTarget target = client.target("http://localhost:8080");
		String conteudo = target.path("/carrinhos/1").request().get(String.class);
		Carrinho carrinho = (Carrinho) new XStream().fromXML(conteudo);
		Assert.assertEquals("Avenida Presidente Vargas 500, 2 andar", carrinho.getRua());
	}
	
	@Test
	public void deveInserirUmNovoCarrinhoCorretamente(){
		WebTarget target = client.target("http://localhost:8080");
		
		Carrinho carrinho = new Carrinho();
		carrinho.adiciona(new Produto(314l, "Tablet",500.0, 1));
		carrinho.setRua("Avenida Presidente Vargas");
		carrinho.setCidade("Rio de Janeiro");
		String carrinhoXml = carrinho.toXML();
		
		Entity<String> entity = Entity.entity(carrinhoXml,MediaType.APPLICATION_XML);
		
		Response response = target.path("/carrinhos").request().post(entity);
		Assert.assertEquals(201,response.getStatus());
		
		String location = response.getHeaderString("Location");
		String conteudo = client.target(location).request().get(String.class);
		Assert.assertTrue(conteudo.contains("Tablet"));
	}
}
